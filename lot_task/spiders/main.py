
import scrapy
import re
import json
from decimal import Decimal, InvalidOperation
from datetime import date


class MainSpider(scrapy.Spider):

    name = 'quoka.de'
    allowed_domains = ['quoka.de']
    start_urls = [
        'http://www.quoka.de/immobilien/bueros-gewerbeflaechen/',
    ]

    POST_DATA = {
        'pricelow': 'von',
        'pricetop': 'bis',
        'customerdisp': '',
        'search1': '',
        'searchbool': 'and',
        'search2': '',
        'city': '',
        'citycheck': '',
        'cityerr': '',
        'citydisp': '',
        'vtlat': '0',
        'vtlong': '0',
        'cityid': '0',
        'zip': '',
        'radius': '25',
        'catid': '27_2710',
        'classtype': 'of',
        'searchhelp': '',
        'logref': '',
        'showallresults': 'false',
        'mask': 'default',
        'sorting': 'adsort',
        'result': 'small',
        'dispads': '20',
        'only': '',
        'pageno': '1',
        'notepageno': '1',
        'mode': 'search',
        'comm': 'all',
        'adsource': '',
        'label': '',
        'suburbid': '0',
        'suburb': '',
        'cityname': '',
        'tlc': '0',
        'searchbutton': 'true',
        'showmorecatlink': 'true',
        'price': '',
        # 'adnolist': '158463742,158463332,140364997,138724963,138349639,157526884,156894813,116629536,118355606,158414322,154520440,143093954,139129311,134282107,134288353,149450491,157643688,153510951,48700423,157488671',
        'detailgeosearch': 'true',
        # 'pnno': 1,
        # 'pnur': /immobilien/bueros-gewerbeflaechen/,
        # 'pnuu': /immobilien/bueros-gewerbeflaechen/,
        # 'pndu': /kleinanzeigen/cat_27_2710_ct_0_page_2.html,
        # 'pntp': 277,
    }

    def __init__(self, *args, **kwargs):
        self.visited_pages = set()

    def start_requests(self):
        url = self.start_urls[0]
        return [
            scrapy.FormRequest(
                url,
                method='POST',
                formdata=self.POST_DATA,
                callback=self.search_page
            )
        ]

    def search_page(self, response):
        for res in self._parse_pagination(response):
            yield res

        for res in self._parse_objects(response):
            yield res

    def _parse_pagination(self, response):
        pages = response.css('div.rslt-pagination li.pageno a.t-pgntn-blue::attr(href)').extract()
        for page in pages:
            yield scrapy.FormRequest(
                'http://www.quoka.de/kleinanzeigen/{}'.format(page),
                callback=self.search_page
            )

    def _parse_objects(self, response):
        objects = response.css('div#ResultListData li.hlisting').extract()
        for div in objects:
            yield self._parse_object(div)

        return  # disable immobilien
        script_src = response.xpath("//script[contains(.//text(), 'getJSON')]").extract_first()
        reg_res = re.search('getJSON\(\s?\'(?P<url>[^\']+)\'\,\s\{(?P<post>[^\}]+)\}?', script_src)
        data = reg_res.groupdict()
        url = data['url']
        post_src = data['post']
        src_elems = [elem.split(':') for elem in post_src.split(',')]
        clear_item = lambda txt: txt[1:-1].replace('\\/', '/')
        post_data = dict([(clear_item(elem[0]), clear_item(elem[1])) for elem in src_elems])
        yield scrapy.FormRequest(
            url,
            method='GET',
            formdata=post_data,
            callback=self.immobilien_objects
        )

    def _clean_price_im(self, value):
        if not value:
            return
        try:
            return Decimal(
                value.
                replace(',', '.').
                replace('EUR', '').
                replace('-', '00')
            )
        except (TypeError, InvalidOperation):
            return

    def _clean_price(self, value):
        if not value:
            return
        try:
            return Decimal(
                value.
                replace(',-', '').
                replace('.', '').
                replace('EUR', '')
            )
        except (TypeError, InvalidOperation):
            return

    def _clean_obid(self, obid):
        reg_res = re.search(r'(?P<id>\d+)', obid)
        if reg_res:
            return reg_res.groupdict()['id']

    def _clean_date(self, date_src):
        reg_res = re.search(r'(?P<day>\d{2})\.(?P<month>\d{2})\.(?P<year>\d{4})', date_src)
        if reg_res:
            data = reg_res.groupdict()
            return date(int(data['year']), int(data['month']), int(data['day']))

    def immobilien_objects(self, response):
        data = json.loads(response.body)
        results = data.get('result', [])
        for res in results:
            result = {
                'Anbieter_ID': 'Immobilienscout',
                'Ueberschrift': res['title'],
                'Beschreibung': res['description'],
                'Kaufpreis': self._clean_price_im(res['priceTotal']),
                # 'PLZ': res['locationZipCode'],  # strange values
                'Stadt': res['locationCity'],
            }
            yield result

    def _parse_object(self, div_html):
        selector = scrapy.Selector(text=div_html)
        detailt_url = selector.css('a.qaheadline::attr(href)').extract_first()
        return scrapy.FormRequest(
            'http://www.quoka.de/{}'.format(detailt_url),
            callback=self.detail_page
        )

    def detail_page(self, response):
        type_ = response.css('div.crmbs span span.category::text').extract()[1]
        title = response.css('div.headline h2::text').extract_first()
        description = response.css('div.details div.text::text').extract_first()
        price = (
            response.css('div.price span::text').extract_first() or
            response.css('div.price strong::text').extract_first() or
            response.css('div.price div::text').extract_first()
        )
        # TODO: phone
        zipcode = response.css('div.location span.postal-code::text').extract_first()
        city = response.css('div.location span.locality::text').extract_first()
        if city:
            city = city.split(' ')[0]
        obid = response.css('div.date-and-clicks strong::text').extract_first()
        dt = response.css('div.date-and-clicks::text').extract()[6]

        result = {
            'Immobilientyp': type_,
            'Ueberschrift': title,
            'Beschreibung': description,
            'Kaufpreis': self._clean_price(price),
            'PLZ': zipcode,
            'Stadt': city,
            'OBID': self._clean_obid(obid),
            'Erstellungsdatum': self._clean_date(dt),
        }
        return result

    def parse(self, response):
        self.logger.info('A response from %s just arrived!', response.url)
