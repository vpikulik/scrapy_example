# -*- coding: utf-8 -*-
from datetime import date
from sqlalchemy import (
    create_engine, MetaData, Table, Column, Integer, String, Text, Numeric,
    Date)

metadata = MetaData()

date_prefix = date.today().strftime('%Y%m')
table_name = '%s_maklerempfehlung_anbieter' % date_prefix
anbieter_table = Table(
    table_name, metadata,
    Column('id', Integer, primary_key=True, autoincrement=True),
    Column('Anbieter_ID', String(100)),
    Column('OBID', String(100)),
    Column('Erstellungsdatum', Date()),
    Column('Immobilientyp', String(255)),
    Column('Ueberschrift', String(255)),
    Column('Beschreibung', Text()),
    Column('Kaufpreis', Numeric(10, 2)),
    Column('PLZ', String(255)),
    Column('Stadt', String(255)),
)


class LotTaskPipeline(object):

    def __init__(self, *args, **kwargs):

        self.engine = create_engine('postgresql://localhost/lot_task')
        metadata.create_all(self.engine)

    def process_item(self, item, spider):
        self.engine.execute(anbieter_table.insert(), **item)
        return item
